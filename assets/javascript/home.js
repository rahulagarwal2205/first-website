function backgroundColor() {
  let redRandomNumber = Math.floor(Math.random() * 256);
  let greenRandomNumber = Math.floor(Math.random() * 256);
  let blueRandomNumber = Math.floor(Math.random() * 256);

  let rgbColor =
    "rgb(" +
    redRandomNumber +
    ", " +
    greenRandomNumber +
    ", " +
    blueRandomNumber +
    ")";

  console.log(rgbColor);

  document.body.style.backgroundColor = rgbColor;
}
let timeout;

function myFunction() {
  timeout = setTimeout(showPage, 2000);
  document.getElementById("container").style.display = "none";
}

function showPage() {
  document.getElementById("loading").style.display = "none";
  document.getElementById("container").style.display = "block";
}

